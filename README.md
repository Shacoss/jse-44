#Task Manager

##Mirroring repositories

https://github.com/Shacoss/JSE

##Требования к  SOFTWARE

- OPENJDK 11

##Стек технологий

- Apache Maven

##Разработчик

Name: Koptev Anton

Email: koptev_av@nlmk.com

##Команды для сборки приложения

`mvn clean` - Удаление всех созданных в процессе сборки артефактов

`mvn compile` - Компилирование проекта

`mvn test` - Тестирование с помощью JUnit тестов

`mvn install` - Копирование .jar (war , ear) в локальный репозиторий

`mvn deploy` - Публикация файла в удалённый репозиторий

Модуль task-manager-server

Локальный запуск приложения:
1. Добавить в переменную среду параметры TASK_MANAGER_DB_URL, TASK_MANAGER_DB_USERNAME, TASK_MANAGER_DB_PASSWORD
2. Выпонить `mvn clean install`

Docker
Создание Docker образа
В корневой папке проекта выполнить `docker build . -t task-manager:v1`

Создание контейнера:
`winpty docker run -it --name task-manager-container -p 8080:8080 task-manager:v1`

Остановить работу контейнера:
`docker stop task-manager-container`

Подключиться к работающему контейнеру:




`mvn liquibase:update` - Создание/Обновление схемы БД

``mvn liquibase:rollback -Dliquibase.rollbackCount=N` - Удалить N changeset







