package ru.koptev.jse;

import jakarta.xml.ws.Endpoint;
import ru.koptev.jse.controller.AccountController;
import ru.koptev.jse.controller.CustomerController;
import ru.koptev.jse.controller.impl.AccountControllerImpl;
import ru.koptev.jse.controller.impl.CustomerControllerImpl;
import ru.koptev.jse.dto.mapper.AccountMapper;
import ru.koptev.jse.dto.mapper.CustomerMapper;
import ru.koptev.jse.repository.AccountRepository;
import ru.koptev.jse.repository.CustomerRepository;
import ru.koptev.jse.repository.impl.AccountRepositoryImpl;
import ru.koptev.jse.repository.impl.CustomerRepositoryImpl;
import ru.koptev.jse.service.AccountService;
import ru.koptev.jse.service.CustomerService;

public class Application {

    public static void main(String[] args) {
        CustomerMapper customerMapper = CustomerMapper.INSTANCE;
        AccountMapper accountMapper = AccountMapper.INSTANCE;
        CustomerRepository customerRepository = new CustomerRepositoryImpl();
        AccountRepository accountRepository = new AccountRepositoryImpl();
        CustomerService customerService = new CustomerService(customerRepository, customerMapper);
        AccountService accountService = new AccountService(accountRepository, customerRepository, accountMapper);
        CustomerController customerController = new CustomerControllerImpl(customerService);
        AccountController accountController = new AccountControllerImpl(accountService);
        Endpoint.publish("http://localhost:8089/ws/customer", customerController);
        Endpoint.publish("http://localhost:8089/ws/account", accountController);
    }

}
