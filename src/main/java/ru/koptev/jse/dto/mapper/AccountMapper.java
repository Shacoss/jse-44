package ru.koptev.jse.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.koptev.jse.dto.AccountDTO;
import ru.koptev.jse.model.Account;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    Account accountDTOtoAccount(AccountDTO accountDTO);

    AccountDTO accountToAccountDTO(Account account);

}
