package ru.koptev.jse.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.koptev.jse.dto.CreateCustomerDTO;
import ru.koptev.jse.dto.CustomerDTO;
import ru.koptev.jse.dto.UpdateCustomerDTO;
import ru.koptev.jse.model.Customer;

import java.util.List;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    Customer customerDTOtoCustomer(CustomerDTO customerDTO);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    Customer customerDTOtoCustomer(CreateCustomerDTO customerDTO);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    Customer customerDTOtoCustomer(UpdateCustomerDTO customerDTO);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    CustomerDTO customerToCustomerDTO(Customer customer);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    List<CustomerDTO> customerListToCustomerListDTO(List<Customer> customers);

}
