package ru.koptev.jse.dto;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "AccountNumber")
    private String accountNumber;
    @XmlElement(name = "Balance")
    private Double ballance;
    @XmlElement(name = "CustomerId")
    private Long customerId;

}
