package ru.koptev.jse.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    private Long id;
    private LocalDate birthDate;
    private String firstName;
    private String lastName;
    private Account account;

}
