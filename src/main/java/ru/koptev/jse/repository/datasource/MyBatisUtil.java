package ru.koptev.jse.repository.datasource;

import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

@Log4j2
public class MyBatisUtil {

    private static final SqlSessionFactory sqlSessionFactory = buildSqlSessionFactory();
    private static final String CONFIG = "mybatis-config.xml";

    private static SqlSessionFactory buildSqlSessionFactory() {
        try (InputStream inputStream = Resources.getResourceAsStream(CONFIG)) {
            return new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            log.error(e);
        }
        return null;
    }

    public static SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    private MyBatisUtil() {

    }

}
