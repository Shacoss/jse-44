package ru.koptev.jse.repository;

import ru.koptev.jse.model.Account;

import java.util.Optional;

public interface AccountRepository {

    Optional<Account> create(Account account);

    Optional<Account> getByIdAndAccountNumber(Account account);

    void update(Account account);

    void delete(Account account);

}
