package ru.koptev.jse.repository;

import ru.koptev.jse.model.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository {

    Optional<Customer> create(Customer customer);

    Optional<Customer> getById(Long id);

    List<Customer> getCustomer(Customer customer);

    void update(Customer customer);

    void delete(Long id);

}
