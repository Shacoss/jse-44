package ru.koptev.jse.repository.impl;

import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.session.SqlSession;
import ru.koptev.jse.model.Customer;
import ru.koptev.jse.repository.CustomerRepository;
import ru.koptev.jse.repository.datasource.MyBatisUtil;
import ru.koptev.jse.repository.impl.mapper.CustomerMapper;

import java.util.List;
import java.util.Optional;

@Log4j2
public class CustomerRepositoryImpl implements CustomerRepository {

    @Override
    public Optional<Customer> create(Customer customer) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
            customerMapper.create(customer);
            session.commit();
            return Optional.of(customer);
        }
    }

    @Override
    public Optional<Customer> getById(Long id) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
            if (customerMapper.getById(id) == null) {
                return Optional.empty();
            }
            return Optional.of(customerMapper.getById(id));
        }
    }

    @Override
    public List<Customer> getCustomer(Customer customer) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
            return customerMapper.getCustomer(customer);
        }
    }

    @Override
    public void update(Customer customer) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
            customerMapper.update(customer);
            session.commit();
        }
    }

    @Override
    public void delete(Long id) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
            customerMapper.delete(id);
            session.commit();
        }
    }

}
