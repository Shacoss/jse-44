package ru.koptev.jse.repository.impl;

import org.apache.ibatis.session.SqlSession;
import ru.koptev.jse.model.Account;
import ru.koptev.jse.repository.AccountRepository;
import ru.koptev.jse.repository.datasource.MyBatisUtil;
import ru.koptev.jse.repository.impl.mapper.AccountMapper;

import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {
    @Override
    public Optional<Account> create(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);
            accountMapper.create(account);
            session.commit();
            return Optional.of(account);
        }
    }

    @Override
    public Optional<Account> getByIdAndAccountNumber(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);
            if (accountMapper.getByIdAndAccountNumber(account) == null) {
                return Optional.empty();
            }
            return Optional.of(accountMapper.getByIdAndAccountNumber(account));
        }
    }

    @Override
    public void update(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);
            accountMapper.update(account);
            session.commit();
        }
    }

    @Override
    public void delete(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);
            accountMapper.delete(account);
            session.commit();
        }
    }

}
