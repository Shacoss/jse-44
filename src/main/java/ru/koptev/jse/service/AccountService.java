package ru.koptev.jse.service;

import ru.koptev.jse.dto.AccountDTO;
import ru.koptev.jse.dto.OperationStatus;
import ru.koptev.jse.dto.ResponseAccountDTO;
import ru.koptev.jse.dto.mapper.AccountMapper;
import ru.koptev.jse.enumerated.Status;
import ru.koptev.jse.model.Account;
import ru.koptev.jse.model.Customer;
import ru.koptev.jse.repository.AccountRepository;
import ru.koptev.jse.repository.CustomerRepository;

import java.util.Optional;

public class AccountService {

    private static final String EMPTY_ID = "Empty id";
    private static final String EMPTY_ACCOUNT_NUMBER = "Empty account number";
    private static final String EMPTY_BALANCE = "Empty balance";
    private static final String NOT_FOUND = "Account not found";
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final AccountMapper mapper;

    public AccountService(AccountRepository accountRepository, CustomerRepository customerRepository, AccountMapper mapper) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        this.mapper = mapper;
    }

    public ResponseAccountDTO create(AccountDTO accountDTO) {
        if (accountDTO.getCustomerId() == null) {
            return errorBuilder(EMPTY_ID);
        }
        if (accountDTO.getAccountNumber() == null || accountDTO.getAccountNumber().isEmpty()) {
            return errorBuilder(EMPTY_ACCOUNT_NUMBER);
        }
        if (accountDTO.getBallance() == null) {
            return errorBuilder(EMPTY_BALANCE);
        }
        Optional<Customer> customerOptional = customerRepository.getById(accountDTO.getCustomerId());
        if (customerOptional.isEmpty()) {
            return errorBuilder("Customer does not exist with id " + accountDTO.getCustomerId());
        }
        Optional<Account> duplicate = accountRepository.getByIdAndAccountNumber(mapper.accountDTOtoAccount(accountDTO));
        if (duplicate.isPresent()) {
            return errorBuilder("The account number already exists with this customer");
        }
        Optional<Account> accountOptional = accountRepository.create(mapper.accountDTOtoAccount(accountDTO));
        if (accountOptional.isPresent()) {
            return responseBuilder(accountOptional.get());
        }
        return errorBuilder("Creation customer failed");
    }

    public ResponseAccountDTO update(AccountDTO accountDTO) {
        if (accountDTO.getCustomerId() == null) {
            return errorBuilder(EMPTY_ID);
        }
        if (accountDTO.getAccountNumber() == null || accountDTO.getAccountNumber().isEmpty()) {
            return errorBuilder(EMPTY_ACCOUNT_NUMBER);
        }
        if (accountDTO.getBallance() == null) {
            return errorBuilder(EMPTY_BALANCE);
        }
        Optional<Account> accountOptional = accountRepository.getByIdAndAccountNumber(mapper.accountDTOtoAccount(accountDTO));
        if (accountOptional.isEmpty()) {
            return errorBuilder(NOT_FOUND);
        }
        accountRepository.update(mapper.accountDTOtoAccount(accountDTO));
        return responseBuilder();
    }

    public ResponseAccountDTO delete(AccountDTO accountDTO) {
        if (accountDTO.getCustomerId() == null) {
            return errorBuilder(EMPTY_ID);
        }
        if (accountDTO.getAccountNumber() == null || accountDTO.getAccountNumber().isEmpty()) {
            return errorBuilder(EMPTY_ACCOUNT_NUMBER);
        }
        Optional<Account> accountOptional = accountRepository.getByIdAndAccountNumber(mapper.accountDTOtoAccount(accountDTO));
        if (accountOptional.isEmpty()) {
            return errorBuilder(NOT_FOUND);
        }
        accountRepository.delete(mapper.accountDTOtoAccount(accountDTO));
        return responseBuilder();
    }

    private ResponseAccountDTO errorBuilder(String message) {
        return ResponseAccountDTO.builder()
                .operationStatus(OperationStatus.builder().status(Status.ERROR).message(message).build())
                .build();
    }

    private ResponseAccountDTO responseBuilder() {
        return ResponseAccountDTO.builder()
                .operationStatus(OperationStatus.builder().status(Status.OK).build())
                .build();
    }

    private ResponseAccountDTO responseBuilder(Account account) {
        return ResponseAccountDTO.builder()
                .accountDTO(mapper.accountToAccountDTO(account))
                .build();
    }

}
