package ru.koptev.jse.controller;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;
import ru.koptev.jse.dto.CreateCustomerDTO;
import ru.koptev.jse.dto.CustomerDTO;
import ru.koptev.jse.dto.ResponseCustomerDTO;
import ru.koptev.jse.dto.UpdateCustomerDTO;

@WebService(serviceName = "CustomerService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CustomerController {

    @WebMethod(operationName = "CreateCustomer")
    @WebResult(name = "Result")
    ResponseCustomerDTO createCustomer(@WebParam(name = "Customer") CreateCustomerDTO customerDTO);

    @WebMethod(operationName = "DeleteCustomer")
    @WebResult(name = "Result")
    ResponseCustomerDTO deleteCustomer(@WebParam(name = "Id") Long id);

    @WebMethod(operationName = "UpdateCustomer")
    @WebResult(name = "Result")
    ResponseCustomerDTO updateCustomer(@WebParam(name = "Customer") UpdateCustomerDTO customerDTO);

    @WebMethod(operationName = "FindCustomer")
    @WebResult(name = "Result")
    ResponseCustomerDTO getCustomer(@WebParam(name = "Customer") CustomerDTO customerDTO);

}
