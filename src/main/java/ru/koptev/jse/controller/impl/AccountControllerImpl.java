package ru.koptev.jse.controller.impl;

import jakarta.jws.WebService;
import ru.koptev.jse.controller.AccountController;
import ru.koptev.jse.dto.AccountDTO;
import ru.koptev.jse.dto.ResponseAccountDTO;
import ru.koptev.jse.service.AccountService;

@WebService(serviceName = "AccountService",
        portName = "AccountServicePort",
        endpointInterface = "ru.koptev.jse.controller.AccountController")
public class AccountControllerImpl implements AccountController {

    private final AccountService accountService;

    public AccountControllerImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public ResponseAccountDTO createAccount(AccountDTO accountDTO) {
        return accountService.create(accountDTO);
    }

    @Override
    public ResponseAccountDTO deleteAccount(AccountDTO accountDTO) {
        return accountService.delete(accountDTO);
    }

    @Override
    public ResponseAccountDTO updateAccount(AccountDTO accountDTO) {
        return accountService.update(accountDTO);
    }

}
