package ru.koptev.jse.controller.impl;

import jakarta.jws.WebService;
import ru.koptev.jse.controller.CustomerController;
import ru.koptev.jse.dto.CreateCustomerDTO;
import ru.koptev.jse.dto.CustomerDTO;
import ru.koptev.jse.dto.ResponseCustomerDTO;
import ru.koptev.jse.dto.UpdateCustomerDTO;
import ru.koptev.jse.service.CustomerService;

@WebService(serviceName = "CustomerService",
        portName = "CustomerServicePort",
        endpointInterface = "ru.koptev.jse.controller.CustomerController")
public class CustomerControllerImpl implements CustomerController {

    private final CustomerService customerService;

    public CustomerControllerImpl(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public ResponseCustomerDTO createCustomer(CreateCustomerDTO customerDTO) {
        return customerService.create(customerDTO);
    }

    @Override
    public ResponseCustomerDTO deleteCustomer(Long id) {
        return customerService.delete(id);
    }

    @Override
    public ResponseCustomerDTO updateCustomer(UpdateCustomerDTO customerDTO) {
        return customerService.update(customerDTO);
    }

    @Override
    public ResponseCustomerDTO getCustomer(CustomerDTO customerDTO) {
        return customerService.findCustomer(customerDTO);
    }
}
