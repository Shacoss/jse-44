package ru.koptev.jse.controller;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;
import ru.koptev.jse.dto.AccountDTO;
import ru.koptev.jse.dto.ResponseAccountDTO;

@WebService(serviceName = "AccountService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AccountController {

    @WebMethod(operationName = "CreateAccount")
    @WebResult(name = "Result")
    ResponseAccountDTO createAccount(@WebParam(name = "Account") AccountDTO accountDTO);

    @WebMethod(operationName = "DeleteAccount")
    @WebResult(name = "Result")
    ResponseAccountDTO deleteAccount(@WebParam(name = "Account") AccountDTO accountDTO);

    @WebMethod(operationName = "UpdateAccount")
    @WebResult(name = "Result")
    ResponseAccountDTO updateAccount(@WebParam(name = "Account") AccountDTO accountDTO);

}
